#include "GraphicDisplay.hpp"

void GraphicDisplay::update()
{
	this->handleEvents();
	SDL_SetRenderDrawColor(GraphicDisplay::renderer, 255, 255, 255, 255);
	SDL_RenderClear(GraphicDisplay::renderer);
	for(int i = this->_modules.size(); i; i--)
	{
		this->_modules[i - 1]->getImage()->setY(((i - 1) * 400));
		this->_modules[i - 1]->displayGraphic();
	}
	SDL_RenderPresent(GraphicDisplay::renderer);

}

void GraphicDisplay::addModule(IMonitorModule *module){
	module->setImage(new SDLImage(GraphicDisplay::renderer, 400, 400, 0, 0));
	module->getImage()->setRenderWidth(400);
	module->getImage()->setRenderHeight(400);

	this->_modules.push_back(module);
}
