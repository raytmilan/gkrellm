#ifndef CPUINFOMODULE_H
# define CPUINFOMODULE_H

#include "IMonitorModule.hpp"
# include <sys/sysctl.h>
#include <vector>
#include <iostream>
#include <sstream>
#include <string>

class CPUInfoModule : public IMonitorModule
{
public:
	CPUInfoModule(void);
	CPUInfoModule(CPUInfoModule const &other);
	CPUInfoModule	&operator=(CPUInfoModule const &other);
	CPUInfoModule(std::string const name);
	virtual ~CPUInfoModule(void);

	void							update(void);
	void							displayGraphic(void){};
	void							displayText(void){};
	std::vector<std::string> const	&getStats(void) const;
	std::string const				&getName(void) const;
	int const						&getSize(void) const;

private:
	std::vector<std::string>		_stats;
	std::string						_name;
	int								_size;
};

#endif
