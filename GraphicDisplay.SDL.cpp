#include "GraphicDisplay.hpp"
#include <SDL2/SDL_ttf.h>
SDL_Renderer *GraphicDisplay::renderer = nullptr;

GraphicDisplay::GraphicDisplay(std::string title, int width, int height) : _title(title),
				_width(width), _height(height), _isRunning(true)
{
	this->init(this->_title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
	this->_width, this->_height, false);
	SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" );
	return ;
}

GraphicDisplay::GraphicDisplay(GraphicDisplay const &other)
{
	*this = other;
	return;
}

GraphicDisplay	&GraphicDisplay::operator=(GraphicDisplay const &)
{
	return (*this);
}

GraphicDisplay::~GraphicDisplay()
{
	return ;
}


bool GraphicDisplay::isRunning()
{
	return this->_isRunning;
}
void	GraphicDisplay::init(std::string title, int x, int y, int width, int height,
															bool fullScreen)
{
	int flags = 0;
	if(fullScreen){
		flags = SDL_WINDOW_FULLSCREEN;
	}

	if(SDL_Init(SDL_INIT_EVERYTHING) == 0)
	{
		TTF_Init();
		std::cout << "GraphicDisplay: Subsystems Initialized." << std::endl;
		this->_window = SDL_CreateWindow(title.c_str(), x, y, width, height, flags);
		if(this->_window)
			std::cout << "GraphicDisplay: Window created." << std::endl;
		GraphicDisplay::renderer = SDL_CreateRenderer(this->_window, -1, 0);
		if(GraphicDisplay::renderer){
			SDL_SetRenderDrawColor(GraphicDisplay::renderer, 255, 255, 255, 255);
			std::cout << "GraphicDisplay: Window created." << std::endl;
		}
	}else
		this->_isRunning = false;
	return;
}

void GraphicDisplay::handleEvents()
{
	SDL_Event event;
	SDL_PollEvent(&event);
	if(event.type == SDL_QUIT)
		this->_isRunning = false;
}

void GraphicDisplay::drawLine(int x1, int y1, int x2, int y2){
	DRAWLINE(GraphicDisplay::renderer, x1, y1, x2, y2);
}

void GraphicDisplay::drawPoint(int x, int y){
	DRAWPOINT(GraphicDisplay::renderer, x, y);
}

void GraphicDisplay::setRendererDrawColor(int r, int g, int b, int a){
	SDL_SetRenderDrawColor(GraphicDisplay::renderer, r, g, b, a);
}

void GraphicDisplay::setRenderTarget(SDL_Texture *texture){
	SDL_SetRenderTarget(GraphicDisplay::renderer, texture);
}

void GraphicDisplay::clean()
{
	SDL_DestroyWindow(this->_window);
	SDL_DestroyRenderer(GraphicDisplay::renderer);
	SDL_Quit();
	std::cout << "GraphicDisplay: Systems Cleaned. 1" << std::endl;
	TTF_Quit();
	std::cout << "GraphicDisplay: Systems Cleaned." << std::endl;
}

void GraphicDisplay::writeText(int x, int y, std::string text, int size, int r, int g,
																		int b)
{
	TTF_Font* Sans = TTF_OpenFont("OpenSans-Regular.ttf", size); //this opens a font style and sets a size
	SDL_Color White = {r, g, b, 0};
	const char * textcstr = (text).c_str();
	SDL_Surface* surfaceMessage = TTF_RenderText_Solid(Sans, textcstr, White);
	SDL_Texture* Message = SDL_CreateTextureFromSurface(renderer, surfaceMessage); //now you can convert it into a texture
	int texW;
	int texH;
	SDL_QueryTexture(Message, NULL, NULL, &texW, &texH);
	SDL_Rect dstrect = { x, y, texW, texH };
	SDL_RenderCopy(renderer, Message, NULL, &dstrect); //you put the renderer's name first, the Message, the crop size(you can ignore this if you don't want to dabble with cropping), and the rect which is the size and coordinate of your texture
	 SDL_DestroyTexture(Message);
	 SDL_FreeSurface(surfaceMessage);
	 TTF_CloseFont(Sans);
}
