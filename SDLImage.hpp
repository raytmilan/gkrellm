/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SDLImage.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sgardner <stephenbgardner@gmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/07 18:34:53 by sgardner          #+#    #+#             */
/*   Updated: 2018/07/08 15:18:39 by sgardner         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SDLIMAGE_HPP
# define SDLIMAGE_HPP
# include <SDL2/SDL.h>

class	SDLImage
{

public:

	SDLImage(void);
	SDLImage(SDL_Renderer *renderer, int width, int height);
	SDLImage(SDL_Renderer *renderer, int width, int height, int x, int y);
	SDLImage(SDLImage const &obj);
	~SDLImage(void);
	SDLImage		&operator=(SDLImage const &obj);

	void			clear(void);
	void			draw(void);

	SDL_Texture		*getTexture(void) const;
	SDL_Renderer	*getRenderer(void) const;
	void			setRenderer(SDL_Renderer *renderer);

	SDL_Rect const	*getSrcRect(void) const;
	int				getWidth(void) const;
	int				getHeight(void) const;

	SDL_Rect const	*getDstRect(void) const;
	int				getRenderWidth(void) const;
	void			setRenderWidth(int width);
	int				getRenderHeight(void) const;
	void			setRenderHeight(int height);
	int				getX(void) const;
	void			setX(int x);
	int				getY(void) const;
	void			setY(int y);

private:

	void			_init(void);

	SDL_Texture		*_texture;
	SDL_Renderer	*_renderer;
	SDL_Rect		_srcRect;
	SDL_Rect		_dstRect;
};
#endif
