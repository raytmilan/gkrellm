#include "DateTimeModule.hpp"
#include "GraphicDisplay.hpp"

DateTimeModule::DateTimeModule(std::string const name) :
	IMonitorModule(), _stats(), _name(name), _size(1)
{
	this->update();
	return ;
}

DateTimeModule::DateTimeModule(DateTimeModule const &other)
{
	*this = other;
	return;
}

DateTimeModule	&DateTimeModule::operator=(DateTimeModule const &)
{
	return (*this);
}

DateTimeModule::~DateTimeModule()
{
	return ;
}

std::vector<std::string> const		&DateTimeModule::getStats(void) const
{
	return (this->_stats);
}

std::string const					&DateTimeModule::getName(void) const
{
	return (this->_name);
}

std::string resize(std::string str)
{
	if (str.size() < 2)
	{
		str = "0" + str;
		str.resize(2);
	}
	return (str);
}

void								DateTimeModule::update(void)
{
	time_t rawtime;
	struct tm *ts;
	std::string	month[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
	std::string	week[] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
	rawtime = time(&rawtime);
	ts = localtime(&rawtime);
	std::stringstream ss;
	std::string mon = month[ts->tm_mon];
	std::string wk = week[ts->tm_wday];
	std::string hour = resize(std::to_string(ts->tm_hour));
	std::string min = resize(std::to_string(ts->tm_min));
	std::string sec = resize(std::to_string(ts->tm_sec));
	ss << wk << " " << mon << " " << ts->tm_year + 1900
		<< " " << hour << ":" << min << ":" << sec;
	this->_stats.push_back(ss.str());
}

int const							&DateTimeModule::getSize(void) const
{
	return (this->_size);
}

void								DateTimeModule::displayGraphic(void)
{
	this->_image->clear();
	GraphicDisplay::setRenderTarget(this->_image->getTexture());
	GraphicDisplay::writeText(0, 0,"Date: " + this->_stats[this->_stats.size() - 1], 24,0,0,255);
	GraphicDisplay::setRenderTarget(nullptr);
	this->_image->draw();
}
