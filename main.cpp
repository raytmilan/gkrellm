#include "CPUUsageModule.hpp"
#include "HostnameModule.hpp"
#include "OSInfoModule.hpp"
#include "RAMUsageModule.hpp"
#include "NetworkUsageModule.hpp"
#include "DateTimeModule.hpp"
#include "GraphicDisplay.hpp"
#include <unistd.h>
#include <ctime>

int main(int argc, char **argv){
	if(argc > 6  || argc < 2 || (strcmp(argv[1], "text") != 0 &&
											strcmp(argv[1], "graphic") != 0)){
		std::cout << "Usage: " << "./ft_gkrellm mode" << std::endl;
		std::cout << "\ttext\n\tgraphic" << std::endl;
		std::cout << "\tmodules" << std::endl;
		return 0;
	}
	//Start main.
	clock_t previousTime = clock();

	//Define Modules
	CPUUsageModule testCPU("CPU");
	RAMUsageModule RAMModule("RAM");
	NetworkUsageModule NetworkModule("Network");
	HostnameModule HostnameModule("Hostname Module");
	OSInfoModule OSInfoModule("OS Info Module");
	DateTimeModule DateTime("Date Info Module");
	IMonitorDisplay *display = nullptr;
	display = new GraphicDisplay("New", 400, 1080);
	for(int i = 0; i < 4 && i < argc - 2; i++)
	{
		if(strcmp(argv[2 + i], "CPU") == 0)
			display->addModule(&testCPU);
		else if(strcmp(argv[2 + i], "RAM") == 0)
			display->addModule(&RAMModule);
		else if(strcmp(argv[2 + i], "Network") == 0)
			display->addModule(&NetworkModule);
		else if(strcmp(argv[2 + i], "Hostname") == 0)
			display->addModule(&HostnameModule);
		else if(strcmp(argv[2 + i], "OS") == 0)
			display->addModule(&OSInfoModule);
		else if(strcmp(argv[2 + i], "DateTime") == 0)
			display->addModule(&DateTime);
	}

	while(display->isRunning())
	{
		display->update();
		if(double(clock() - previousTime) / CLOCKS_PER_SEC >= 0.1)
		{
			testCPU.update();
			RAMModule.update();
			NetworkModule.update();
			DateTime.update();
			previousTime = clock();
		}
	}
	display->clean();
	delete display;
}
