#include "RAMUsageModule.hpp"
#include "GraphicDisplay.hpp"

RAMUsageModule::RAMUsageModule(std::string const name) :
	IMonitorModule(), _stats(), _name(name), _size(2)
{
	system("mkdir monitorFiles > /dev/null 2 > /dev/null");
	this->update();
	return ;
}

RAMUsageModule::RAMUsageModule(RAMUsageModule const &other)
{
	*this = other;
	return;
}

RAMUsageModule::~RAMUsageModule()
{
	return ;
}

RAMUsageModule						&RAMUsageModule::operator=(RAMUsageModule const &)
{
	return (*this);
}

std::vector<std::string> const		&RAMUsageModule::getStats(void) const
{
	return (this->_stats);
}

std::string const					&RAMUsageModule::getName(void) const
{
	return (this->_name);
}

void								RAMUsageModule::update(void)
{
	if(this->_stats.size() > 100)
		this->_stats.clear();
	system((RAMUsageModule::_getInfoSysCommand).c_str());
	this->addFileInfoToStats(this->readStatsFile());
	return;
}

int const							&RAMUsageModule::getSize(void) const
{
	return (this->_size);
}

const std::string					RAMUsageModule::readStatsFile(void) const
{
	std::ifstream file;
	file.open(RAMUsageModule::_monitorFilePath);
	std::string line = "";
	if (file.is_open())
	{
		std::getline(file, line);
		file.close();
	}
	else
	{
		line = "PhysMem: ERROR used (ERROR wired), ERROR unused.";
	}
	return line;
}

void								RAMUsageModule::addFileInfoToStats(std::string info)
{
	std::string token;
	size_t pos = info.find(": ") + 2;
	size_t end = info.find(" used");
	token = info.substr(pos, end - pos);
	this->_stats.push_back(token); // used

	pos = info.find(", ", end) + 2;
	end = info.find(" unused", pos);
	token = info.substr(pos, end - pos);
	this->_stats.push_back(token); // unused
}

void								RAMUsageModule::displayGraphic(void)
{
	this->_image->clear();
	GraphicDisplay::setRenderTarget(this->_image->getTexture());
	GraphicDisplay::setRendererDrawColor(0,255,255,255);
	static float totalRAM = 0;
	int graphWidth = this->_image->getWidth();
	int graphHeight = this->_image->getHeight();
	int xOffset = 0;
	int yOffset = 40;
	int pX = 0;
	int pY = 0;
	if (totalRAM == 0)
		totalRAM = std::stof(this->_stats.at(this->_stats.size() - 1)) + std::stof(this->_stats.at(this->_stats.size() - 2));
	GraphicDisplay::writeText(0, 0, "RAM usage: ", 24, 0,0, 0);
	int statsSize = this->_stats.size();
	for (int i = 0; i < 30 && i < statsSize; i += 2){
			if(i == 0){
				pX = xOffset + 0 + (i/3 * 20);
				pY = yOffset + (std::stof(this->_stats[(statsSize - i - 1)]) *
												graphHeight / totalRAM);
				GraphicDisplay::drawPoint(pX, pY);
			}
			else
			{
				GraphicDisplay::drawLine(pX, pY, xOffset +  0 + (i/3 * graphWidth / 10), yOffset +
					(std::stof(this->_stats[(statsSize - 1) - i]) * graphHeight / totalRAM));
				pX = xOffset +  0 + (i/3 * graphWidth / 10);
				pY = yOffset + (std::stof(this->_stats[(statsSize - 1) - i]) *
												graphHeight / totalRAM);
			}
		// GraphicDisplay::putBarRAM(x, y + 10,
			// (this->_stats.size() - i) / 2 * 3, 100 * std::stof(this->_stats.at(i)) / totalRAM);
	}
	GraphicDisplay::writeText(0, 140, this->_stats.at(this->_stats.size() - 2), 24, 0, 0, 0);
	GraphicDisplay::setRenderTarget(nullptr);
	this->_image->draw();
	GraphicDisplay::setRendererDrawColor(255,255,255,255);
}

const std::string RAMUsageModule::_monitorFilePath = "monitorFiles/ramUsage.monitor";
const std::string RAMUsageModule::_getInfoSysCommand = "top -l 1 | grep \"PhysMem:\" > " + RAMUsageModule::_monitorFilePath;
