#include "IMonitorModule.hpp"

IMonitorModule::IMonitorModule(void) : _image(nullptr)
{
	return;
}

IMonitorModule::~IMonitorModule(void)
{
	return;
}

IMonitorModule::IMonitorModule(IMonitorModule const &other)
{
	*this = other;
	return;
}

IMonitorModule	&IMonitorModule::operator=(IMonitorModule const &)
{
	return (*this);
}

SDLImage						*IMonitorModule::getImage()
{
	return this->_image;
}

void							IMonitorModule::setImage(SDLImage *image)
{
	this->_image = image;
	return ;
}
